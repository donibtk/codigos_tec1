# gem install openssl

require 'openssl'

# gera um array com toda as combinações de 'aaaa' até 'zzzz' (com repetições)
@dictionary = ('a'..'z').to_a.repeated_permutation(4).map(&:join)
#palavras para pesquisar
words_to_search = ['alma', 'esta', 'jaca', 'orca', 'tatu', 'zero']

class String
    def sha1
        OpenSSL::Digest::SHA1.hexdigest self
    end
    def sha512
        OpenSSL::Digest::SHA512.hexdigest self
    end
    def md5
      OpenSSL::Digest::MD5.hexdigest self
    end
end

def find_md5(word_hash)
  for i in (0..(@dictionary.length - 1)) do
    return i if @dictionary[i].md5 == word_hash
  end
  puts "Erro ao procurar MD5"
end

def find_sha1(word_hash)
  for i in (0..(@dictionary.length - 1)) do
    return i if @dictionary[i].sha1 == word_hash
  end
  puts "Erro ao procurar SHA1"
end

def find_sha512(word_hash)
  for i in (0..(@dictionary.length - 1)) do
    return i if @dictionary[i].sha512 == word_hash
  end
  puts "Erro ao procurar SHA512"
end

# garante que os modulos foram carregados em memoria
"teste".sha1
"teste".sha512
"teste".md5

words_to_search.each do |w|
  puts "Palavra: '#{w}'"

  start = Time.now
  index = find_md5 w.md5
  total = (Time.now - start) * 1000
  printf "Tempo MD5: %d, Indice: %d\n", total.round(0), index

  start = Time.now
  index = find_sha1 w.sha1
  total = (Time.now - start) * 1000
  printf "Tempo SHA1: %d, Indice: %d\n", total.round(0), index

  start = Time.now
  index = find_sha512 w.sha512
  total = (Time.now - start) * 1000
  printf "Tempo SHA512: %d, Indice: %d\n", total.round(0), index

end
